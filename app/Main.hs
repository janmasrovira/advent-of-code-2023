module Main where

import Base
import AllDays
import Prelude (read)

main :: IO ()
main = do
  a <- getArgs
  case a of
    nday : npart : _ -> do
      let parts = dayParts (dayFromNat (read nday))
      case npart of
          "1" -> parts ^. _part1
          "2" -> parts ^. _part2
          _ -> missingPart (pack nday) (pack npart)
    _ -> error "Provide the number of the day and the part"

missingDay :: Text -> a
missingDay day = error ("Day " <> day <> " not implemented")

missingPart :: Text -> Text -> a
missingPart day part = error ("Day " <> day <> ", part " <> part <> " not implemented")
