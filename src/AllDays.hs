module AllDays where

import Base
import Day1 qualified
import Day2 qualified
import Day3 qualified
import Day4 qualified
import Day5 qualified
import Day12 qualified

dayFromNat :: Natural -> Day
dayFromNat n
  | 1 <= n && n <= 25 = toEnum (pred (fromIntegral n))
  | otherwise = error "Day number must be between 1 and 25"

data Day =
 Day1
 | Day2
 | Day3
 | Day4
 | Day5
 | Day6
 | Day7
 | Day8
 | Day9
 | Day10
 | Day11
 | Day12
 | Day13
 | Day14
 | Day15
 | Day16
 | Day17
 | Day18
 | Day19
 | Day20
 | Day21
 | Day22
 | Day23
 | Day24
 | Day25
 deriving stock (Bounded, Enum, Eq)

dayParts :: Day -> DayParts
dayParts = \case
 Day1 -> Day1.parts
 Day2 -> Day2.parts
 Day3 -> Day3.parts
 Day4 -> Day4.parts
 Day5 -> Day5.parts
 Day6 -> emptyParts
 Day7 -> emptyParts
 Day8 -> emptyParts
 Day9 -> emptyParts
 Day10 -> emptyParts
 Day11 -> emptyParts
 Day12 -> Day12.parts
 Day13 -> emptyParts
 Day14 -> emptyParts
 Day15 -> emptyParts
 Day16 -> emptyParts
 Day17 -> emptyParts
 Day18 -> emptyParts
 Day19 -> emptyParts
 Day20 -> emptyParts
 Day21 -> emptyParts
 Day22 -> emptyParts
 Day23 -> emptyParts
 Day24 -> emptyParts
 Day25 -> emptyParts
