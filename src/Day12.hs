module Day12 (parts) where

import Extra

data Status
  = Known Known
  | Unknown
  deriving stock (Show, Eq)

data Known
  = Ok
  | Broken
  deriving stock (Show, Eq, Bounded, Enum)

status :: Char -> Maybe Status
status = \case
  '.' -> Just (Known Ok)
  '#' -> Just (Known Broken)
  '?' -> Just Unknown
  _ -> Nothing

data Row = Row
  { _rowElems :: NonEmpty Status,
    _rowNumbers :: NonEmpty Natural
  }

type Seq = [Known]

data Builder = Builder
  { _builderSeqs :: [Seq],
    _builderComplete :: Bool
  }
  deriving stock (Show)

makeLenses ''Row
makeLenses ''Builder

parts :: DayParts
parts = DayParts (Just part1) (Just part2)

part2 :: IO ()
part2 = part True

part1 :: IO ()
part1 = part False

part :: Bool -> IO ()
part shouldUnfold = do
  i <- getContents
  let res :: Natural = sum . map (goRow . unfoldIf . parseRow) . lines $ i
  putStrLn (show res)
  where
    unfoldIf :: Row -> Row
    unfoldIf
      | shouldUnfold = \r ->
          let fiveTimes :: NonEmpty x -> NonEmpty x
              fiveTimes = nonEmpty' . concat . replicate 5 . toList
           in Row
                { _rowElems = fiveTimes (r ^. rowElems),
                  _rowNumbers = fiveTimes (r ^. rowNumbers)
                }
      | otherwise = id

emptyBuilder :: Builder
emptyBuilder =
  Builder
    { _builderSeqs = [[]],
      _builderComplete = False
    }

append :: Seq -> Known -> Seq
append s k = s ++ [k]

stepBuilder :: (Members '[Reader [Natural], State Builder] r) => Status -> Sem r ()
stepBuilder s = do
  modify' (over builderSeqs step)
  filterBuilder
  where
    step :: [Seq] -> [Seq]
    step bs =
      let ks = case s of
            Unknown -> allElements
            Known k -> [k]
       in concat [[append b k | k <- ks] | b <- bs]

filterBuilder :: forall r. (Members '[Reader [Natural], State Builder] r) => Sem r ()
filterBuilder = modifyM (traverseOf builderSeqs (filterM compatible))
  where
    compatible :: Seq -> Sem r Bool
    compatible seq = do
      nats <- ask
      fmap isJust (runFail (evalState seq (go nats)))
      where
        go :: forall r'. (r' ~ (State [Known] ': Fail ': r)) => [Natural] -> Sem r' ()
        go ms = do
          b <- get
          case ms of
            []
              | b ^. builderComplete -> failUnlessM (all (== Ok) <$> get @[Known])
              | otherwise -> return ()
            n : ns -> do
              modify' (dropWhile (== Ok))
              s <- get
              let (br, rest) = span (== Broken) s
              if
                | b ^. builderComplete -> failUnless (len br == n)
                | null rest -> failUnless (len br <= n)
                | otherwise -> failUnless (len br == n)
              put rest
              go ns

runBuilder :: (Members '[Reader [Natural]] r) => Sem (State Builder ': r) () -> Sem r Natural
runBuilder m = evalState emptyBuilder $ do
  m
  modify' (set builderComplete True)
  filterBuilder
  len <$> gets (^. builderSeqs)

len :: (Foldable f) => f a -> Natural
len = fromIntegral . length

goRow :: Row -> Natural
goRow r = run . runReader (toList (r ^. rowNumbers)) . runBuilder $ do
  forM_ (r ^. rowElems) stepBuilder

parseRow :: Text -> Row
parseRow = parse pRow
  where
    pRow :: Parser Row
    pRow = do
      _rowElems <- some1 pStatus
      n <- natural
      ns <- many (symbol "," >> natural)
      let _rowNumbers = n :| ns
      return Row {..}

    pStatus :: Parser Status
    pStatus = lexeme (token status mempty)
