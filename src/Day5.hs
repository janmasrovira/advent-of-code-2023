module Day5 (parts) where

import Extra
import Prelude qualified

newtype Category = Category
  { _category :: Text
  }
  deriving stock (Eq, Generic)

instance Show Category where
  show (Category t) = unpack t

instance Hashable Category

data Rules = Rules
  { _rulesSource :: Category,
    _rulesDestination :: Category,
    _rulesEntries :: [Entry]
  }

data Entry = Entry
  { _entryDestination :: Natural,
    _entrySourceStart :: Natural,
    _entryRange :: Natural
  }

newtype RulesTable = RulesTable
  { _rulesTable :: HashMap Category Rules
  }

data Almanac = Almanac
  { _almanacSeeds :: NonEmpty Natural,
    _almanacRules :: RulesTable
  }

-- | Non empty inclusive range
data Range = Range
  { _rangeLeft :: Natural,
    _rangeLength :: Natural
  }

mkRangeFromLength' :: Natural -> Natural -> Range
mkRangeFromLength' a len =
  fromJust
    . runFail'
    $ mkRangeFromLength a len

mkRangeFromLength :: (Members '[Fail] r) => Natural -> Natural -> Sem r Range
mkRangeFromLength a len = do
  failUnless (len > 0)
  return (Range a len)

mkRangeFromInterval :: (Members '[Fail] r) => Natural -> Natural -> Sem r Range
mkRangeFromInterval a b = do
  len <- subtract (b + 1) a
  mkRangeFromLength a len

makeLenses ''Range
makeLenses ''Rules
makeLenses ''Entry
makeLenses ''RulesTable
makeLenses ''Almanac

instance Show Range where
  show r = Prelude.show (r ^. rangeLeft) <> " ... " <> Prelude.show (r ^. rangeRight)

rangeRight :: SimpleGetter Range Natural
rangeRight = to (\r -> r ^. rangeLeft + r ^. rangeLength - 1)

parts :: DayParts
parts = DayParts (Just part1) (Just part2)

part2 :: IO ()
part2 = do
  g <- getContents
  let a = parseAlmanac g
      initialSeeds = nonEmpty' (run (execOutputList (fromPairs (toList (a ^. almanacSeeds)))))
      res = run (runReader (a ^. almanacRules) (goSeeds initialSeeds))
  print res
  where
    fromPairs :: [Natural] -> Sem '[Output Range] ()
    fromPairs = \case
      [] -> return ()
      a : b : xs -> do
        output (mkRangeFromLength' a b)
        fromPairs xs
      _ -> impossible

sRange :: Natural -> Range
sRange a = fromJust (runFail' (mkRangeFromInterval a a))

part1 :: IO ()
part1 = do
  g <- getContents
  let a = parseAlmanac g
      res = run (runReader (a ^. almanacRules) (goSeeds (sRange <$> a ^. almanacSeeds)))
  print res

parseAlmanac :: Text -> Almanac
parseAlmanac = parse pAlmanac
  where
    pCategory :: Parser Category
    pCategory = lexeme (Category <$> takeWhile1P Nothing isLetter)

    pEntry :: Parser Entry
    pEntry = do
      _entryDestination <- natural
      _entrySourceStart <- natural
      _entryRange <- natural
      return Entry {..}

    pRules :: Parser Rules
    pRules = do
      _rulesSource <- pCategory
      chunk "-to-"
      _rulesDestination <- pCategory
      symbol "map:"
      _rulesEntries <- some pEntry
      return Rules {..}

    pRulesTable :: Parser RulesTable
    pRulesTable = do
      rules <- many pRules
      return RulesTable {_rulesTable = hashMapBy (^. rulesSource) rules}

    pAlmanac :: Parser Almanac
    pAlmanac = do
      symbol "seeds:"
      _almanacSeeds <- some1 natural
      _almanacRules <- pRulesTable
      eof
      return Almanac {..}

seed :: Category
seed = Category "seed"

location :: Category
location = Category "location"

matchEntries2 :: Range -> [Entry] -> NonEmpty Range
matchEntries2 n es =
  fromMaybe impossible
    . nonEmpty
    . run
    . execOutputList
    . evalState (Just n)
    . ignoreFail
    $ helper
  where
    helper :: forall r. (Members '[Output Range, State (Maybe Range), Fail] r) => Sem r ()
    helper = do
      mapM_ go (sortOn (^. entrySourceStart) es)
      outputRest
      where
        getRange :: Sem r Range
        getRange = get >>= failMaybe

        outputRest :: Sem r ()
        outputRest = getRange >>= output

        entry2Range :: Entry -> Range
        entry2Range e =
          let l = e ^. entrySourceStart
           in fromJust (runFail' (mkRangeFromLength l (e ^. entryRange)))

        go :: Entry -> Sem r ()
        go e = do
          s <- getRange
          let re = entry2Range e
              (l, c, r) = matchRanges s re
          whenJust l output
          whenJust c $ \c' -> do
            let coff = fromJust (runFail' (subtract (c' ^. rangeLeft) (re ^. rangeLeft)))
            output (set rangeLeft (e ^. entryDestination + coff) c')
          put r

-- | (Left, Overlapping, Right)
matchRanges :: Range -> Range -> (Maybe Range, Maybe Range, Maybe Range)
matchRanges a b =
  let l = runFail' $ do
        b' <- naturalPred (b ^. rangeLeft)
        let leftRight = min (a ^. rangeRight) b'
        mkRangeFromInterval (a ^. rangeLeft) leftRight
      overLapRight = min (a ^. rangeRight) (b ^. rangeRight)
      c = runFail' $ do
        let overLapLeft = max (a ^. rangeLeft) (b ^. rangeLeft)
        mkRangeFromInterval overLapLeft overLapRight
      r = runFail' $ do
        let rightLeft = max (a ^. rangeLeft) (succ (b ^. rangeRight))
            rightRight = a ^. rangeRight
        mkRangeFromInterval rightLeft rightRight
   in (l, c, r)

goSeeds :: (Members '[Reader RulesTable] r) => NonEmpty Range -> Sem r Natural
goSeeds = fmap getMin . execOutputSemigroup' . mapM_ (goSeed seed)

goSeed :: forall r. (Members '[Reader RulesTable, Output (Min Natural)] r) => Category -> Range -> Sem r ()
goSeed c n
  | c == location = storeLoc (n ^. rangeLeft)
  | otherwise = do
      r :: Rules <- asks (^?! rulesTable . at c . _Just)
      let n' = matchEntries2 n (r ^. rulesEntries)
          c' = r ^. rulesDestination
      mapM_ (goSeed c') n'
  where
    storeLoc :: Natural -> Sem r ()
    storeLoc = output . Min
