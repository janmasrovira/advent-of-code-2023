module Day1 where

import Base
import Data.Text qualified as Text

charToNat :: Char -> Natural
charToNat = readNatural . pack . pure

recoverLine :: Text -> Natural
recoverLine l =
  let findNat = charToNat . fromMaybe impossible . Text.find isDigit
      a = findNat l
      b = findNat (Text.reverse l)
   in 10 * a + b

part1 :: IO ()
part1 = do
  i <- getContents
  let res :: Natural = sum . map recoverLine . lines $ i
  putStrLn (show res)

digits :: [(Natural, Text)]
digits = zip [1 ..] ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]

data St = St
  { _stFirst :: First Natural,
    _stLast :: Last Natural
  }

makeLenses ''St

part2 :: IO ()
part2 = do
  i <- getContents
  let res :: Natural = sum . map goLine . lines $ i
  putStrLn (show res)
  where
  goLine :: Text -> Natural
  goLine = getFinalNumber . run . execState mempty .  runParserT parseLine

instance Semigroup St where
  (St f l) <> (St f' l') = St (f <> f') (l <> l')

instance Monoid St where
  mempty = St mempty mempty

getFinalNumber :: St -> Natural
getFinalNumber (St f l) =
  let a = fromMaybe' (getFirst f)
      b = fromMaybe' (getLast l)
   in 10 * a + b

parseLine :: forall m. m ~ ParsecT (Sem '[State St]) => m ()
parseLine = skipMany parseToken >> eof
  where
  parseToken :: m ()
  parseToken =
    parseDigitChar
    <|> parseDigitWord
    <|> ignoreAnyChar
    where
      rememberNumber :: Natural -> m ()
      rememberNumber n = lift $ do
        modify' (over stFirst (<> pure n))
        modify' (over stLast (<> pure n))

      parseDigitWord :: m ()
      parseDigitWord = do
        lookAhead (choice [ chunk w >> rememberNumber n | (n, w) <- digits ])
        ignoreAnyChar

      parseDigitChar :: m ()
      parseDigitChar = satisfy isDigit >>= rememberNumber . charToNat

      ignoreAnyChar :: m ()
      ignoreAnyChar = void anySingle

parts :: DayParts
parts = DayParts (Just part1) (Just part2)
