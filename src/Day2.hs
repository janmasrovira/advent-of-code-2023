module Day2 where

import Extra

data Bag = Bag
  { _red :: Natural,
    _green :: Natural,
    _blue :: Natural
  }

data Color
  = Red
  | Green
  | Blue

data Game = Game
  { _gameId :: Natural,
    _gameSets :: [CubeSet]
  }

newtype CubeSet = CubeSet
  { _cubeSet :: [Move]
  }

data Move = Move
  { _moveColor :: Color,
    _moveNum :: Natural
  }

makeLenses ''Bag
makeLenses ''Game
makeLenses ''Move
makeLenses ''CubeSet

bagColor :: Color -> Lens' Bag Natural
bagColor = \case
  Red -> red
  Green -> green
  Blue -> blue

bag :: Bag
bag =
  Bag
    { _red = 12,
      _green = 13,
      _blue = 14
    }

parts :: DayParts
parts = DayParts (Just part1) (Just part2)

colors :: [(Text, Color)]
colors = [("red", Red), ("blue", Blue), ("green", Green)]

parseGame :: Text -> Game
parseGame = parse pGame
  where
    pGame :: Parser Game
    pGame = do
      symbol "Game"
      _gameId <- natural
      symbol ":"
      _gameSets <- sepBy pSet (symbol ";")
      eof
      return Game {..}
    pColor :: Parser Color
    pColor = choice [symbol w $> c | (w, c) <- colors]
    pMove :: Parser Move
    pMove = do
      _moveNum <- natural
      _moveColor <- pColor
      return Move {..}
    pSet :: Parser CubeSet
    pSet = do
      _cubeSet <- sepBy pMove (symbol ",")
      return CubeSet {..}

playGame :: Game -> Maybe Natural
playGame g = run . runReader bag . runFail $ do
  forM_ (g ^. gameSets) checkSet
  return (g ^. gameId)

checkSet :: (Members '[Fail, Reader Bag] r) => CubeSet -> Sem r ()
checkSet m = forM_ (m ^. cubeSet) checkMove

checkMove :: (Members '[Fail, Reader Bag] r) => Move -> Sem r ()
checkMove m = do
  b <- asks (^. bagColor (m ^. moveColor))
  void (subtract b (m ^. moveNum))

goLine :: Text -> Maybe Natural
goLine = playGame . parseGame

part1 :: IO ()
part1 = do
  i <- getContents
  let res :: Natural = sum . mapMaybe goLine . lines $ i
  putStrLn (show res)

power :: Bag -> Natural
power (Bag a b c) = a * b * c

goLine2 :: Text -> Natural
goLine2 = playGame2 . parseGame

playGame2 :: Game -> Natural
playGame2 g = power . run . execState emptyBag $ do
  forM_ (g ^. gameSets) goSet
  where
    emptyBag :: Bag
    emptyBag = Bag 0 0 0

    goSet :: CubeSet -> Sem '[State Bag] ()
    goSet c = forM_ (c ^. cubeSet) goMove

    goMove :: Move -> Sem '[State Bag] ()
    goMove m = modify' (over (bagColor (m ^. moveColor)) (max (m ^. moveNum)))

part2 :: IO ()
part2 = do
  i <- getContents
  let res :: Natural = sum . map goLine2 . lines $ i
  putStrLn (show res)
