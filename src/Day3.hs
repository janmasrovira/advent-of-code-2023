module Day3 where

import Data.Text qualified as Text
import Extra

data Symbol = Symbol
  { _symbolPos :: Natural,
    _symbolIsGear :: Bool
  }

data Number = Number
  { _numberStart :: Natural,
    _numberValue :: Natural,
    _numberEnd :: Natural
  }

data Item
  = ItemSymbol Symbol
  | ItemNumber Number

newtype Row = Row
  { _rowItems :: [Item]
  }

newtype Grid = Grid
  { _gridRows :: [Row]
  }

data Near a = Near
  { _prevLine :: [a],
    _thisLine :: [a],
    _nextLine :: [a]
  }
type NearSymbols = Near Symbol
type NearNumbers = Near Number

makeLenses ''Grid
makeLenses ''Row
makeLenses ''Number
makeLenses ''Symbol
makeLenses ''Near

parts :: DayParts
parts = DayParts (Just part1) (Just part2)

parseRow :: Text -> Row
parseRow = run . evalState 0 . runParserT helper
  where
    helper :: forall m. (m ~ ParsecT (Sem '[State Natural])) => m Row
    helper = do
      pDots
      _rowItems <- many pItem
      eof
      return Row {..}
      where
        advance :: Natural -> m ()
        advance n = lift (modify' (+ n))

        pDots :: m ()
        pDots = skipMany pDot

        pDot :: m ()
        pDot = do
          void (single '.')
          advance 1

        getPos :: m Natural
        getPos = lift get

        pSymbol :: m Symbol
        pSymbol = do
          _symbolPos <- getPos
          c <- anySingleBut '.'
          let _symbolIsGear = c == '*'
          advance 1
          return Symbol {..}

        pNumber :: m Number
        pNumber = do
          _numberStart <- getPos
          _numberValue <- natural
          let n = fromIntegral (Text.length (show _numberValue))
              _numberEnd = _numberStart + n - 1
          advance n
          return Number {..}

        pItem :: m Item
        pItem = do
          i <-
            ItemNumber
              <$> pNumber
              <|> ItemSymbol
              <$> pSymbol
          pDots
          return i

near :: forall a. (Item -> Maybe a) -> Grid -> [Near a]
near getA g =
  [ Near p t n
    | (p, t, n) <- zip3Exact ([] : init symbols) symbols (drop 1 symbols ++ [[]])
  ]
  where
    symbols :: [[a]]
    symbols = map (^.. rowItems . each . to getA . _Just) (g ^. gridRows)

nearNumbers :: Grid -> [NearNumbers]
nearNumbers = near getNum

nearSymbols :: Grid -> [NearSymbols]
nearSymbols = near getSym

predNatural :: Natural -> Natural
predNatural n
  | n == 0 = 0
  | otherwise = pred n

matches :: Number -> Symbol -> Bool
matches n s = predNatural (n ^. numberStart) <= sp && sp <= succ (n ^. numberEnd)
  where
  sp = s ^. symbolPos

matchesGear :: Symbol -> Number -> Bool
matchesGear = flip matches

symbolLeftOf :: Number -> Symbol -> Bool
symbolLeftOf n s = s ^. symbolPos < predNatural (n ^. numberStart)

numberLeftOf :: Symbol -> Number -> Bool
numberLeftOf s n = n ^. numberEnd < predNatural (s ^. symbolPos)

findGridGears :: Grid -> [Natural]
findGridGears g =
  let ns = nearNumbers g
      z = zipExact (g ^. gridRows) ns
  in concatMap (uncurry findRowGears) z

findGridNumbers :: Grid -> [Number]
findGridNumbers g =
  let ns = nearSymbols g
      z = zipExact (g ^. gridRows) ns
  in concatMap (uncurry findRowNumbers) z

findRowGears :: Row -> NearNumbers -> [Natural]
findRowGears r n = run (evalState n (mapMaybeM isGear (r ^.. rowItems . each . to getSym . _Just)))
  where
  isGear :: forall r. Members '[State NearNumbers] r => Symbol -> Sem r (Maybe Natural)
  isGear sym = runFail $ do
    ns <- gearNumbers
    failUnless (sym ^. symbolIsGear)
    case map (^. numberValue) ns of
      [a, b] -> return (a * b)
      _ -> fail
    where
    gearNumbers :: forall r'. Members '[State NearNumbers] r' => Sem r' [Number]
    gearNumbers = do
      let dropLeft l = modify' (over l (dropWhile (numberLeftOf sym)))
      dropLeft prevLine
      dropLeft thisLine
      dropLeft nextLine
      ns <- get
      let takeNumbers = takeWhile (matchesGear sym)
          nums = concatMap takeNumbers [ns ^. prevLine, ns ^. thisLine, ns ^. nextLine]
      return nums

findRowNumbers :: Row -> NearSymbols -> [Number]
findRowNumbers r n = run (evalState n (filterM isPartNumber (r ^.. rowItems . each . to getNum . _Just)))
  where
  isPartNumber :: forall r. Members '[State NearSymbols] r => Number -> Sem r Bool
  isPartNumber num = do
    let dropLeft l = modify' (over l (dropWhile (symbolLeftOf num)))
    dropLeft prevLine
    dropLeft thisLine
    dropLeft nextLine
    ns <- get
    let candidates = mapMaybe headMay [ns ^. prevLine, ns ^. thisLine, ns ^. nextLine]
    return (any (matches num) candidates)

parseGrid :: Text -> Grid
parseGrid txt = Grid (map parseRow (lines txt))

getSym :: Item -> Maybe Symbol
getSym = \case
  ItemSymbol n -> Just n
  _ -> Nothing

getNum :: Item -> Maybe Number
getNum = \case
  ItemNumber n -> Just n
  _ -> Nothing

part1 :: IO ()
part1 = do
  g <- parseGrid <$> getContents
  let ns = findGridNumbers g
  putStrLn (show (sum (map (^. numberValue) ns)))

part2 :: IO ()
part2 = do
  g <- parseGrid <$> getContents
  let ns = findGridGears g
  putStrLn (show (sum ns))
